// @flow
import {createStore, combineReducers, applyMiddleware} from "redux";
import thunk from "redux-thunk";
import promise from "redux-promise-middleware";
import articles from "./reducers/ArticleReducer";
import user from './reducers/UserReducer'
import category from './reducers/CategoryReducer'

export default createStore(
    combineReducers({
        articles,
        user,
        category
    }),
    {},
    applyMiddleware(thunk, promise())
);
