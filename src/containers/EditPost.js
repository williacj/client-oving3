//@flow
import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import connect from "react-redux/es/connect/connect";
import {fetchArticles} from "../actions/ArticleAction";
import {fetchCategory} from "../actions/CategoryAction";
// Material UI Components

// Icons

//Components
import Navigation from "../compontents/Navigation";
import EditArticle from "../compontents/ArticleRefac";

const styles = {
    root: {
        width: 'auto',
        backgroundColor: 'whiteSmoke',
        height: '100vh',
    },
    wrapper: {
        display:'grid',
        gridTemplateColumns: '50%',
        justifyContent: 'center',
        padding: '100px 0',
        '@media only screen and (max-width: 800px)': {
            gridTemplateColumns: '100%',
            padding: '10px 0',
        },
    }
};

interface Props {
    classes: Object,
    match: Object,
    articles: Array<Object>,
    loading: boolean,
    error: Object,
    dispatch: Function
}


class EditPost extends Component<Props> {

    componentDidMount(){
        if(!(this.props.articles.length>0)){
            this.props.dispatch(fetchArticles());
            this.props.dispatch(fetchCategory());
        }};


    render() {
        const {classes, articles, loading, error} = this.props;
        const id = this.props.match.params.id;
        let article = articles.find(article => article._id === id);
        return (
            <Navigation>
            {(articles.length>0)?
                <div className={classes.root}>
                    <div className={classes.wrapper}>
                        <EditArticle edit={true} id={id} article={article}/>
                    </div>
                </div>
            : null
            }
            </Navigation>

        );
    }
}


const mapStateToProps = (state: Object) => ({
    articles: state.articles.items,
    loading: state.articles.loading,
    error: state.articles.error
});

const InjectionStyles = withStyles(styles)(EditPost);

export default connect(mapStateToProps)(InjectionStyles);
