//@flow
import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import { withRouter} from "react-router-dom";
import ReactMarkdown from 'react-markdown';
import {fetchUser} from "../service/api/user";

//Redux
import {fetchArticles, updateAnArticle,delArt} from "../actions/ArticleAction";
import {deleteArticle, voteArticle} from "../service/api/Article";
import {fetchCategory} from "../actions/CategoryAction";
import {connect} from "react-redux";

// Project components
import Head from "../compontents/Head"
import Navigation from "../compontents/Navigation";
import Information from "../compontents/Information";
import Voting from '../compontents/Voting'
import Comment from '../compontents/Comment'
import CommentSection from '../compontents/CommentSection'
import Dial from '../compontents/Dial'


//Material UI
import Typography from "@material-ui/core/Typography";
import Paper from '@material-ui/core/Paper';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';

let mock : Object = {
    title: "",
    text: "",
    selectedFile: "",
    subheader: "",
    order: "1",
    cat: "",
};

const styles = {
    root:{
        width:'auto',
        height:'auto',
        backgroundColor:'whiteSmoke',
    },
    header:{
        '@media only screen and (max-width: 800px)': {
            fontSize: 30
        },
    },
    subheader: {
        '@media only screen and (max-width: 800px)': {
            fontSize: 20
        },
    },
    wrapper:{
        paddingTop:'30px',
        paddingBottom:'30px',
        display: 'grid',
        gridTemplateColumns: '60%',
        gridGap:'20px',
        justifyContent:'center',
        '@media only screen and (max-width: 800px)': {
            gridTemplateColumns:  '95%',
        },
    },
    grid:{
        padding:'10px 20px 10px 20px',
        display: 'flex',
        flexDirection:'row',
        flexWrap: 'wrap',
        justifyContent:'space-between',
        alignItems:'center',
    },
    comment :{
        display: 'grid',
        gridGap:'5px',
    },
    mt:{
        '@media only screen and (max-width: 800px)': {
            marginTop: 10
        },
    },
    commentWrapper: {
        padding: '30px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    }

};

type Props = {
    classes: Object,
    match: Object,
    articles: Array<Object>,
    loading: boolean,
    error: Object,
    history: Object,
  fetchArticles: Function,
  fetchCategory: Function,
  updateAnArticle: Function,
  delArt: Function,
}

type State = {
    isLoaded: boolean,
    voted :boolean,
  open: boolean,

}

class Event extends Component<Props,State>{
    state = {
        isLoaded: false,
      voted: false,
      open: false
    };

    componentDidMount(){
        this.fetchAll()
    };


    deleteArticle = async () =>{
        let id = this.props.match.params.id;
        let article = this.props.articles.find(article => article._id === id);
        await deleteArticle(id).then(
          this.props.delArt(article),
          this.props.history.push('/')
        );
    };

    close = () : void =>{
        this.setState({
          open: false,
        })
    };

    open = () =>{
      this.setState({
        open: true,
      })
    };

    fetchAll = async () => {
        if(this.props.articles.length === 0){
            await this.props.fetchArticles();
            await this.props.fetchCategory();
        }
        await this.hasVoted();
    };


    hasVoted = async (): Promise<void> =>{
        let article = this.props.articles.find(article => article._id === this.props.match.params.id);

        if(article) {
            await fetchUser()
                .then(res => res.json())
                .then(json => {
                    const user = article.rate.find(e => e.user === json.user);
                    this.setState({
                        voted: !!user,
                    })
                })
        }
    };


    vote = async ()=> {
        let article : Object;
        let find  = this.props.articles.find(article => article._id === this.props.match.params.id);
        if(find){
            article = find;
            await  voteArticle(article._id);
            await this.props.updateAnArticle(article);
            await this.setState({
                voted: true
            })
        }
    };

    editPage = (id : string) : void=>{
        this.props.history.push("/edit/"+id);
    };


    render() {

        const { articles, classes} = this.props;
        const { id } = this.props.match.params;

        let article : Object = mock;
        let find  = this.props.articles.find(article => article._id === this.props.match.params.id);
        if(find){
            article = find;
        }

        return (
            <Navigation footer>
                {
                    (articles.length>0)?
                        <div className={classes.root}>
                            <div className={classes.wrapper}>
                                <Typography variant='display3' className={classes.header}>{article.title}</Typography>
                                <Typography variant='display1' className={classes.subheader}>{article.subtitle}</Typography>
                                <Head articleImage={article.articleImage}/>

                                <Paper className={classes.grid}>
                                    <Information
                                        article={article}
                                    />
                                    <Chip
                                        label={article.category.name}
                                        color="primary"
                                        variant="outlined"
                                    />
                                    <Voting article={article} number={article.rate.length} voted={this.state.voted} onClick={() => this.vote()}/>
                                </Paper>

                                <div className={classes.text}>
                                    <ReactMarkdown source={article.text}/>
                                </div>
                                <div className={classes.grid}>
                                    <Button variant="outlined" color="secondary" onClick={() => this.open()}>
                                        Delete Article
                                    </Button>

                                    <Button variant="contained" className={classes.mt} onClick={() => this.editPage(id)}> Edit Article</Button>

                                </div>

                                <CommentSection id={id} article={article}/>

                                <div className={classes.comment}>
                                    {article.comments.reverse().map(comment =>{
                                        return <Comment key={comment._id} data={comment}/>
                                    })}
                                </div>
                                <Dial close={this.close} deleteArt={this.deleteArticle} open={this.state.open}/>

                            </div>
                        </div>: <div>Not loaded</div>

                }

            </Navigation>
        );
    }
}



const mapStateToProps = state => ({
    articles: state.articles.items,
    loading: state.articles.loading,
    error: state.articles.error
});

const mapDispatchToProps = (dispatch) => {
  return {
    fetchArticles: () => {
      dispatch(fetchArticles());
    },
    fetchCategory: () =>{
        dispatch(fetchCategory())
    },
    updateAnArticle: (article : Object) =>{
        dispatch(updateAnArticle(article))
    },
    delArt: (article) =>{
        dispatch(delArt(article))
    }

  };
};

const history = withRouter(Event);
const InjectionStyles = withStyles(styles)(history);

export default connect(mapStateToProps,mapDispatchToProps)(InjectionStyles);
