//@flow
import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import {connect} from "react-redux";
import {fetchArticles} from '../actions/ArticleAction'
import {fetchCategory} from '../actions/CategoryAction'
import {filterArticles} from "../service/logical/article";

// Project components
import Navigation from "../compontents/Navigation";
import EventCard from "../compontents/EventCard";
import Newsfeed from '../compontents/Newsfeed'

const styles = {
    root:{
        width:'auto',
        height:'auto',
        backgroundColor:'whiteSmoke'
    },
    wrapper:{
        paddingBottom:'30px',
        display: 'grid',
        gridTemplateColumns: ' 20% 20% 20% ',
        gridTemplateRows:'auto',
        margin:'auto',
        gridGap:'15px',
        order:1,
        justifyContent:'center',
        '@media only screen and (max-width: 800px)': {
            gridTemplateColumns:  '95%',
        },
    },
    head:{
        gridRowStart:1,
        gridColumnStart:1,
        gridColumnEnd:4,
        '@media only screen and (max-width: 800px)': {
            gridColumnEnd:2,
        },
    },

};
interface Props{
    error: Object,
    loading: boolean,
    articles: Array<Object>,
    classes: Object,
    dispatch: Function,
    match: Function
}
interface State {

}
class EventPage extends Component<Props, State> {
    componentDidMount(){
        if(!(this.props.articles.length >0)){
            this.props.dispatch(fetchArticles());
            this.props.dispatch(fetchCategory());
        }
    }

    render() {
        let art : Array<Object>;
        const { error, loading, articles , classes} = this.props;

        if (error) {
            return <div>Error! {error.message}</div>;
        }

        if (loading) {
            return <div>Loading...</div>;
        }
        if(!loading && typeof this.props.match.params.name !== 'undefined'){
            const name = this.props.match.params.name;
            art = articles.filter(e => e.category.name === name);
        }else{
            art = articles.filter(e => e.order === 1);
        }

        return (
            <Navigation footer>
                <div className={classes.root}>
                    <Newsfeed/>
                    <div className={classes.wrapper}>
                        {art.map((value, index) => {
                            if(index === 0){
                                return <EventCard key={value._id} className={classes.head} data={value} title='display2'/>
                            }
                            else{
                                return <EventCard key={value._id} data={value} title='display1'/>
                            }
                        })}


                    </div>
                </div>
            </Navigation>
        );
    }
}

const mapStateToProps = state => ({
    articles: state.articles.items,
    loading: state.articles.loading,
    error: state.articles.error
});

const InjectionStyles = withStyles(styles)(EventPage);

export default connect(mapStateToProps)(InjectionStyles);
