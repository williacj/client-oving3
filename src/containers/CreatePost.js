//@flow
import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';

// Material UI Components

// Icons

//Components
import CreateArticle from '../compontents/ArticleRefac'
import Navigation from "../compontents/Navigation";

const styles = {
    root: {
        width: 'auto',
        backgroundColor: 'whiteSmoke',
        height: '100vh',
    },
    wrapper: {
        display:'grid',
        gridTemplateColumns: '50%',
        justifyContent: 'center',
        padding: '100px 0',
        '@media only screen and (max-width: 800px)': {
            gridTemplateColumns: '100%',
            padding: '10px 0',
        },
    }
};

interface Props {
    classes: Object
}


class CreatePost extends Component<Props> {
    render() {
        const {classes} = this.props;
        return (
            <Navigation>
                <div className={classes.root}>
                    <div className={classes.wrapper}>
                        <CreateArticle />
                    </div>
                </div>
            </Navigation>
        );
    }
}


export default withStyles(styles)(CreatePost);
