//@flow

module.exports = {
      loading: false,
      error: "null",
    articles:[
      {
    _id: "0",
    title: "MongoDB is the new NOSQL",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, se" +
      "d do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco labor" +
      "is nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugia" +
      "t nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    articleImage: "iamge",
    order: "1",
    rate: [],
    comments: [{user: "william", date: Date.now(),text: "this is a nice site!" },
      {user: "william", date: Date.now(),text: "gah!" },
      {user: "jack", date: Date.now(),text: "The oldschool is the best school!" },
      {user: "niels", date: Date.now(),text: "Oh so nice!!" }
    ],
    category: {
      name:"red"
    },
    subtitle: "this is a subtitle!"},

  {
    _id: "1",
    title: "MongoDB is the new NOSQL",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, se" +
      "d do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco labor" +
      "is nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugia" +
      "t nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    articleImage: "iamge",
    order: "1",
    rate: [],
    comments: [{user: "william", date: Date.now(),text: "this is a nice site!" },
      {user: "william", date: Date.now(),text: "gah!" },
      {user: "jack", date: Date.now(),text: "The oldschool is the best school!" },
      {user: "niels", date: Date.now(),text: "Oh so nice!!" }
    ],
    category: {
      name:"red"
    },
    subtitle: "this is a subtitle!"},

  {
    _id: "2",
    title: "MongoDB is the new NOSQL",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, se" +
      "d do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco labor" +
      "is nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugia" +
      "t nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    articleImage: "iamge",
    order: "1",
    category: {
      name: "blue"
    },
    rate: [],
    comments: [{user: "william", date: Date.now(),text: "this is a nice site!" },
      {user: "william", date: Date.now(),text: "gah!" },
      {user: "jack", date: Date.now(),text: "The oldschool is the best school!" },
      {user: "niels", date: Date.now(),text: "Oh so nice!!" }
    ],
    subtitle: "this is a subtitle!"},

  {
    _id: "4",
    title: "MongoDB is the new NOSQL",
    text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, se" +
      "d do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco labor" +
      "is nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugia" +
      "t nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    order: "2",
    rate: [],
    category: {
      name:"blue"
    },
    articleImage: "iamge",

    comments: [{user: "william", date: Date.now(),text: "this is a nice site!" },
      {user: "william", date: Date.now(),text: "gah!" },
      {user: "jack", date: Date.now(),text: "The oldschool is the best school!" },
      {user: "niels", date: Date.now(),text: "Oh so nice!!" }
    ],
    subtitle: "this is a subtitle!"},
]};

