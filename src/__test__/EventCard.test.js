// Article function testing
//@flow
import React from 'react';
import { createShallow } from '@material-ui/core/test-utils'
import EventCard from '../compontents/EventCard';
import './setup'
import mockdata from './__Mock__/article'
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import {Link} from 'react-router-dom';
let wrapper,shallow;

describe('Shallow check of EventPage', ()=>{

    beforeEach(()=>{
        shallow = createShallow();
        wrapper = shallow(<EventCard data={mockdata.articles[0]} title={'display1'} />).dive();
    });

    it('checking if <Card /> exists', ()=>{
        expect(wrapper.find(Card).length).toEqual(1)
    });


  it('checking if <CardMedia /> exists', ()=>{
    expect(wrapper.find(CardMedia).length).toEqual(1)
  });

  it('checking if <Link /> exists', ()=>{
    expect(wrapper.find(Link).length).toEqual(1)
  });

  it('checking if <Link /> clicking', ()=>{
    wrapper.find('Link').simulate('click');
    expect(wrapper.find(Link).length).toEqual(1)
  });


});





