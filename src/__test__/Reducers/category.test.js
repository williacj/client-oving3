//@flow
import reducer from '../../reducers/CategoryReducer';
import {
  FETCH_CATEGORY_BEGIN,
  FETCH_CATEGORY_SUCCESS,
  FETCH_CATEGORY_FAILURE,
} from '../../actions/CategoryAction';

//MOCKDATA
import mockdata from '../__Mock__/category'

describe('Category reducer', () => {

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      {
        category: [],
        loading: false,
        error: null
      }
    )
  })

  test('should FETCH_CATEGORY_SUCCESS', ()=>{
    expect(
      reducer([], {
        type: FETCH_CATEGORY_SUCCESS,
        payload: mockdata,
      })
    ).toEqual(
      {
        category: mockdata.category,
        loading: false,
      })
  })

  test('should FETCH_CATEGORY_FAILURE', ()=>{
    expect(
      reducer([], {
        type: FETCH_CATEGORY_FAILURE,
        payload: mockdata,
      })
    ).toEqual(
      {
        loading: false,
        error: mockdata.error,
        category: []
      })
  })

  test('should FETCH_CATEGORY_BEGIN', ()=>{
    expect(
      reducer([], {
        type: FETCH_CATEGORY_BEGIN,
        payload: mockdata,
      })
    ).toEqual(
      {
        loading: true,
        error: null
      })
  })

});
