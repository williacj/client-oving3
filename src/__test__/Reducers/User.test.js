//@flow
import reducer from '../../reducers/UserReducer';

import {
  FETCH_USER_BEGIN,
  FETCH_USER_SUCCSESS,
  FETCH_USER_FAILURE,
} from '../../actions/UserActions';

//MOCKDATA
import mockdata from '../__Mock__/user'

describe('User reducer', () => {

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      {
        items: null,
        loading: false,
        error: null
      }
    )
  })

  test('should FETCH_USER_SUCCSESS', ()=>{
    expect(
      reducer([], {
        type: FETCH_USER_SUCCSESS,
        payload: mockdata,
      })
    ).toEqual(
      {
        items: mockdata.user,
        loading: false,
      })
  })

  test('should FETCH_USER_FAILURE', ()=>{
    expect(
      reducer([], {
        type: FETCH_USER_FAILURE,
        payload: mockdata,
      })
    ).toEqual(
      {
        loading: false,
        error: mockdata.error,
      })
  })

  test('should FETCH_USER_BEGIN', ()=>{
    expect(
      reducer([], {
        type: FETCH_USER_BEGIN,
        payload: mockdata,
      })
    ).toEqual(
      {
        loading: true,
        error: null
      })
  })

});
