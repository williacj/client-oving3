//@flow
import reducer from '../../reducers/ArticleReducer';
import {
  FETCH_ARTICLES_BEGIN,
  FETCH_ARTICLES_SUCCESS,
  FETCH_ARTICLES_FAILURE,
  UPDATE_ARTICLE,
  DELETE_ARTICLE
} from '../../actions/ArticleAction';

//MOCKDATA
import mockdata from '../__Mock__/article'

describe('Article reducer', () => {

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      {
        items: [],
        loading: false,
        error: null
      }
    )
  })

  test('should FETCH_ARTICLES_SUCSESS', ()=>{
    expect(
      reducer([], {
        type: FETCH_ARTICLES_SUCCESS,
        payload: mockdata,
      })
    ).toEqual(
      {
        items: mockdata.articles,
        loading: false,
      })
  })

  test('should FETCH_ARTICLES_FAILURE', ()=>{
    expect(
      reducer([], {
        type: FETCH_ARTICLES_FAILURE,
        payload: mockdata,
      })
    ).toEqual(
      {
        loading: false,
        error: mockdata.error,
        items: []
      })
  })

  test('should FETCH_ARTICLES_BEGIN', ()=>{
    expect(
      reducer([], {
        type: FETCH_ARTICLES_BEGIN,
        payload: mockdata,
      })
    ).toEqual(
      {
        loading: true,
        error: null
      })
  })

});
