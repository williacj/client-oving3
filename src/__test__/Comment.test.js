// Article function testing
//@flow
import React from 'react';
import { createShallow } from '@material-ui/core/test-utils'
import Comment from '../compontents/Comment';
import './setup'
import mockdata from './__Mock__/article'
import Paper from "@material-ui/core/Paper";
let wrapper,shallow;

describe('Shallow check of EventPage', ()=>{

  beforeEach(()=>{
    shallow = createShallow();
    wrapper = shallow(<Comment data={mockdata.articles[0].comments[0]} title={'display1'} />).dive();
  });

  it('checking if <Paper /> exists', ()=>{
    expect(wrapper.find(Paper).length).toEqual(1)
  });



});
