//@flow
import React, {Component} from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import {withStyles} from '@material-ui/core/styles';


//Pages
import Event from "./containers/Event";
import EventPage from './containers/EventPage'
import SignIn from './containers/SignIn'
import RegisterUser from './containers/RegisterUser';
import CreatePost from './containers/CreatePost'
import EditPost from "./containers/EditPost";

export const url : string= "http://localhost:8000";

const styles ={
  root:{
      width:'100%',
      height:'100%',
      backgroundColor:'whiteSmoke',
  }
};

interface Props  {

}

class App extends Component<Props> {
  render() {
      const{ } = this.props;
    return (
        <BrowserRouter>
            <Switch>
                <Route path={"/"} component={EventPage} exact/>
                <Route path={"/article/:id"} component={Event}/>
                <Route path={"/signIn"} component={SignIn}/>
                <Route path={"/register"} component={RegisterUser}/>
                <Route path={"/createPost"} component={CreatePost}/>
                <Route path={"/Category/:name"} component={EventPage}/>
                <Route path={"/edit/:id"} component={EditPost}/>
            </Switch>
        </BrowserRouter>
    );
  }
}

export default withStyles(styles)(App);
