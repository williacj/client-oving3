//@flow
import {
    FETCH_ARTICLES_BEGIN,
    FETCH_ARTICLES_SUCCESS,
    FETCH_ARTICLES_FAILURE,
    UPDATE_ARTICLE,
    DELETE_ARTICLE
} from '../actions/ArticleAction';

let initialItems = {
    items: [],
    loading: false,
    error: null
};

export default function articleReducer(state :any = initialItems  , action:Object) : Object{
    switch(action.type) {
        case FETCH_ARTICLES_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            };

        case FETCH_ARTICLES_SUCCESS:
            return {
                ...state,
                loading: false,
                items: action.payload.articles
            };

        case FETCH_ARTICLES_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload.error,
                items: []
            };
        case UPDATE_ARTICLE:
            const index : number = state.items.findIndex(e => e._id === action.payload.article._id);
            let items : Array<Object> = Object.assign([],state.items);
            items[index] = action.payload.article;
            return{
                ...state,
                items
            };
      case DELETE_ARTICLE:
            const i : number = state.items.findIndex(e => e._id === action.payload.article._id);
            return{
              ...state,
                items: [
                  ...state.items.slice(0, i), ...state.items.slice(i+ 1)
                ]

            };

        default:
            return state;
    }
}
