//@flow
import {
    FETCH_USER_BEGIN,
    FETCH_USER_SUCCSESS,
    FETCH_USER_FAILURE,
} from '../actions/UserActions';
let initialItems = {
    items: null,
    loading: false,
    error: null
};

export default function userReducer(state:any = initialItems, action:Object) : Object {
    switch(action.type) {
        case FETCH_USER_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            };

        case FETCH_USER_SUCCSESS:
            return {
                ...state,
                loading: false,
              items: action.payload.user
            };

        case FETCH_USER_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload.error,
            };

        default:
            return state;
    }
}
