//@flow
import {
    FETCH_CATEGORY_BEGIN,
    FETCH_CATEGORY_SUCCESS,
    FETCH_CATEGORY_FAILURE,
} from '../actions/CategoryAction';

let initialItems = {
  category: [],
    loading: false,
    error: null
};

export default function articleReducer(state:any = initialItems, action:Object) : Object {
    switch(action.type) {
        case FETCH_CATEGORY_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            };

        case FETCH_CATEGORY_SUCCESS:
            console.log(action.payload.category);
            return {
                ...state,
                loading: false,
                category: action.payload.category
            };

        case FETCH_CATEGORY_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload.error,
              category: []
            };

        default:
            return state;
    }
}
