//@flow
import React  from 'react';
import {withStyles} from '@material-ui/core/styles';

import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";

const styles = {

};

type Props = {
  open: boolean,
  close : Function,
  deleteArt : Function,
}

const Dial = (props : Props) => {
  return (
    <div>
      <Dialog
        open={props.open}
        onClose={props.close}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">{"delete article?"}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Are you sure you want to delete the article?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={props.close} color="primary">
            Disagree
          </Button>
          <Button onClick={props.deleteArt} color="primary" autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
};

export default withStyles(styles)(Dial)
