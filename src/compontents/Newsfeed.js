//@flow
import React, {Component} from 'react';

import {Link} from "react-router-dom";
import moment from 'moment-timezone'
import {connect} from 'react-redux'

// Material UI Components
import {withStyles} from '@material-ui/core/styles';
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import List from "@material-ui/core/List";
// Icons
import ArrowIcon from '@material-ui/icons/KeyboardArrowRight'

const styles = {
    root: {
        height: '100px',
        width: 'auto',
        margin: 0,
        position: 'relative',
        alignItems: 'center'
    },
    list:{
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        overflow: 'auto',
        borderStyle: 'hidden hidden solid hidden',
        borderWidth: '1px',
        borderColor: 'gray'
    },
    listobj :{
        height: 65,
        width: 400,
        textDecoration: 'none',
        borderStyle: 'hidden solid hidden hidden',
        borderWidth: '1px',
        borderColor: 'black'
    },
    arrow: {
        paddingTop: '15px',
        background: 'linear-gradient(to right,rgba(255,255,255,0.1),rgb(255,255,255))',
        height: 60,
        width: '60px',
        position: 'absolute',
        display: 'inline-block',
        right: 0,
        top:0,
        alignItems: 'bottom'
    }
};
type Props ={
    classes: Function,
    articles: Array<Object>,
    error: Object,
    loading: boolean,
}

const Newsfeed =(props : Props) =>{
    const {classes, articles, error, loading} = props;

    return (
            loading ? null :
                <div className={classes.root}>
                    {
                        <List className={classes.list}>
                            <ListItem className={classes.listobj}>
                                <ListItemText primary={"Nyhetssaker"} />
                            </ListItem>
                            {articles.slice(0,6).map((e) => {
                                let id = e._id;
                                return <Link to={'/article/' + id} key={id} className={classes.listobj}>
                                    <ListItem key={id} button className={classes.listobj}>
                                    <ListItemText primary={e.title} secondary= {moment(e.time).tz('Europe/Oslo').format('DD/MM/YYYY, HH:mm')} />
                                    </ListItem>
                                </Link>
                            })}
                            </List>
                    }
                    <div className={classes.arrow}>
                        <ArrowIcon fontSize='large'/>
                    </div>
                </div>
    );
};

const mapStateToProps = state => ({
    articles: state.articles.items,
    loading: state.articles.loading,
    error: state.articles.error
});

const InjectionStyles = withStyles(styles)(Newsfeed);

export default connect(mapStateToProps)(InjectionStyles);

