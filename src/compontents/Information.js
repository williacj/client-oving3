import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import classNames from "classnames";
import moment from 'moment-timezone'

import Typography from '@material-ui/core/Typography';

const styles = {
    root: {
        width: 'auto',
        height: 'auto',
        margin: 0,
        fontFamily:'arial'
    }
};

type Prop ={
    classes: Object,
    article : Object,
    edited: Date,
    className: Object
}

const Information = (props : Prop) =>{
        const { classes,article } = props;
        const i  = moment(article.time);
        const edit = moment(article.edited);
        return (
            <div className={classNames(classes.root, props.className)}>
                <div>
                    <Typography> Writer: {article.writer.name}</Typography>
                    <Typography> Published: {i.tz('Europe/Oslo').format('DD/MM/YYYY, HH:mm')}</Typography>
                    {typeof article.edited !== 'undefined' ? <Typography> Edited: {edit.tz('Europe/Oslo').format('DD/MM/YYYY, HH:mm')}</Typography> : null}
                </div>
            </div>
        );
};


export default withStyles(styles)(Information);
