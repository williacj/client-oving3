//@flow
import React from 'react';
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";

// Material UI Components

// Icons

type Props ={
    data: Object,
}

const NavButton = (props: Props) => {
    const {data} = props;
    return (
        <div>
            <Link to={data.link} style={{ textDecoration: 'none' }}>
                <Button color="inherit" style={{
                    color: 'white',
                }}>
                    {data.text}
                </Button>
            </Link>
        </div>
    );
};

export default (NavButton);


