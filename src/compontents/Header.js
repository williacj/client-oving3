//@flow
import React, {Component, Fragment} from 'react';
import { withStyles } from '@material-ui/core/styles';
import classNames from "classnames";
import {Link} from 'react-router-dom'
import Popup from 'reactjs-popup'
import {fetchCategory} from '../service/api/category'
import {fetchUser} from '../service/api/user'
import {connect} from "react-redux";

//Material UI
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Hidden from '@material-ui/core/Hidden'
import IconButton from '@material-ui/core/IconButton'
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

//Components
import NavButton from './NavButton'


import MenuIcon from '@material-ui/icons/Menu'
const ICON : string = "https://www.sce.ntnu.edu.tw/home/favicon.ico";
const title : string= 'Williams Forum';



const styles = {
    root: {
        display:'flex',
        justifyContent: 'space-evenly',
        alignItems:'center',
        maxHeight:'200px',
        width:'100%',
        flexDirection:'column'
    },
    navWrapper:{
        display:'flex',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        flexDirection:'row',
        padding: '10px 10px 10px 10px'
    },
    iconWrapper:{
        flexGrow:1,
        display:'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection:'row'
    },
    grow:{
        display:'flex',
        flexDirection:'row',
        flexGrow: 17,
        alignItems:'center'
    },
    cell:{
        display:'flex',
        flexDirection:'row',
        alignItems:'center'
    }

};

interface Props {
    classes: Object,
    className: Object
}

interface State {
    loggedIn: boolean,
    loaded: boolean,
    category: Object,
    categories: Array<Object>,
    hamburger: boolean,

}

class Header extends Component<Props,State>{
    state ={
        loggedIn: false,
        loaded: false,
        category: { },
        categories: [],
        hamburger: false,
    };

    componentDidMount(){
        fetchUser().then(result => {
            this.setState({
                loggedIn: result.status === 200
            })
        });
        fetchCategory().then(cat =>{
            this.setState({
                categories: cat,
                loaded: true,
            })
        }).catch(err =>{
            console.log(err)
        });
    }

    toggleDrawer = () : void=> {
        this.setState({
            hamburger: !this.state.hamburger,
        });
    };

    render(){
        const {classes} = this.props;

        return (
            <div className={classNames(classes.root, this.props.className)}>
                <AppBar position="static">
                    <Toolbar className={classes.navWrapper}>

                        <Link to={'/'} style={{ textDecoration: 'none' }}>
                            <div className={classes.iconWrapper}>
                                <img src={ICON} width='60px' />

                                <Typography variant="display1" style={{textDecoration: 'none', color: 'white'}} className={classes.flex}>{title}</Typography>
                            </div>
                        </Link>

                            <Hidden smDown>
                                <div className={classes.grow}>
                                    {
                                    this.state.categories.map(e => {
                                        return <NavButton  key={e._id} data={{link: "/Category/"+e.name, text: e.name}} />
                                    })
                                }
                            </div>
                        </Hidden>
                    <Hidden smDown>
                    {
                        this.state.loggedIn ?
                                <NavButton data={{link: "/CreatePost", text: "Create post"}} />
                        :   <div className={classes.cell}>
                                <NavButton data={{link: "/register", text: "register"}} />
                                <NavButton data={{link: "/signin", text: "login "}} />
                            </div>


                    }
                    </Hidden>
                    <Hidden mdUp>
                        <IconButton onClick={() => this.toggleDrawer()}>
                            <MenuIcon fontSize='large'/>
                        </IconButton>
                    </Hidden>
                        <Popup
                            modal
                            overlayStyle={{ background: "rgba(255,255,255,0.9" }}
                            closeOnDocumentClick={false}
                            open={this.state.hamburger}
                        >
                            {close => <Menu cats={this.state.categories} close={close} loggedIn={this.state.loggedIn}/>}

                        </Popup>
                </Toolbar>
            </AppBar>
        </div>
    );
    }
}

type Props1 ={
    loggedIn: boolean,
    close:Function,
    cats: Array<Object>
}

const Menu = (props : Props1) =>{
    return(
        <List>
            { !props.loggedIn ?
                <Fragment>
                <Link to={"/CreatePost"}>
                    <ListItem button onClick={props.close}>
                        <ListItemText primary='create post'/>
                    </ListItem>
                </Link>
                <Divider/>
                </Fragment>
                :
                <Fragment>
                <Link to={"/signIn"}>
                    <ListItem button onClick={props.close}>
                        <ListItemText primary='Sign in'/>
                    </ListItem>
                    <Divider/>
                </Link>
                <Link to={"/register"}>
                    <ListItem button onClick={props.close}>
                        <ListItemText primary='Regsiter user'/>
                    </ListItem>
                </Link>
                <Divider/>
            </Fragment>
            }

            {props.cats.map(e => {
               return<Fragment key={e.name}>
                    <Link to={"/category/" + e.name}>
                    <ListItem button onClick={props.close}>
                        <ListItemText primary={e.name}/>
                    </ListItem>
                </Link>
                <Divider/>
                </Fragment>
            })}
            <Link to={"/"}>
                <ListItem button onClick={props.close}>
                    <ListItemText primary='HOME'/>
                </ListItem>
            </Link>
        </List>
    )
};

const mapStateToProps = state => ({
    user: state.user.user,
    loading: state.user.loading,
    error: state.user.error,
});


const InjectionStyles = withStyles(styles)(Header);

export default connect(mapStateToProps, null)(InjectionStyles);
