//@flow
import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import classNames from "classnames";

import Paper from '@material-ui/core/Paper'

const styles = {
    root: {
        width: 'auto',
        height: 'auto',
    },
    wrapper:{
        height: '400px',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        '@media only screen and (max-width: 800px)': {
            height:  '200px',
        },
    },
};

type Props ={
    articleImage: string,
    classes: Object,
    className: Object
}


const Head = (props : Props)  =>{
        const { classes, articleImage } = props;

        return (
            <Paper className={classNames(classes.root, props.className)}>
                <div className={classes.wrapper} style={{backgroundImage:`url(${'http://localhost:8000/'+articleImage})`}}/>
            </Paper>
        );

};

export default withStyles(styles)(Head);
