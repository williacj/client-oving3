//@flow
import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import {signIn, fetchUserFailure} from '../actions/UserActions'
import {registerUser} from "../service/api/user";
import Text from '../Textfiles/LogginText'
import connect from "react-redux/es/connect/connect";

// Material UI Components
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
// Icons


const styles = {
    root: {
        height: '500px',
        width: 'auto',
        padding:'20px'
    },
    grid: {
        display:'flex',
        flexDirection: 'column',
        justifyContent:'center',
    },
    cell:{
        margin: '30px  0px'
    },
    welcome: {
        margin: '100px 0px'
    }
};

interface Props {
    classes: Object,
    register: boolean,
    user: Object,
    loading: boolean,
    error: Object,
    signIn: Function
}

interface State {
    username: string,
    password: string,
    callback: Object
}



class Forum extends Component<Props, State> {

    state = {
        username: "",
        password: "",
        callback: { },
    };


    handleChangeUser = (event : SyntheticInputEvent<HTMLInputElement>)  => {
        this.setState({username: event.target.value});
    };

    handleChangePassword = (event : SyntheticInputEvent<HTMLInputElement>) => {
        this.setState({password: event.target.value});
    };

    regUser = (username: string, password: string): void => {
       registerUser(username, password).then(json => {
            this.setState({
                callback : json
            })
        }).catch(error => fetchUserFailure(error));
    };

    render() {
        const {classes, register, user } = this.props;

        return (
            <Paper className={classes.root}>
                {user !== null ?
                    <div className={classes.welcome}>
                        <Typography variant='display3' align='center'>Welcome : </Typography>
                        <Typography variant='display2' align='center'>{user.user} </Typography>
                    </div>
                    :

                <forum className={classes.grid}>
                    <Typography align='center' variant='display1'>{register ? "Register" : "Logg in"}</Typography>
                    <Typography variant='subheading' >Username: </Typography>
                    <TextField
                        label="Username"
                        placeholder="Please enter username"
                        margin="normal"
                        variant="outlined"
                        value={this.state.username} onChange={this.handleChangeUser}
                        required
                    />
                    <Typography variant='subheading'>password: </Typography>
                    <TextField
                        label="Password"
                        type="password"
                        autoComplete="current-password"
                        margin="normal"
                        variant="outlined"
                        value={this.state.password} onChange={this.handleChangePassword}
                        required
                    />
                    {register ? <div/> :
                        <Button color='primary' type='submit' variant='contained' onClick={() => {this.props.signIn(this.state.username, this.state.password)}} >Logg in</Button>}
                    {register ? <div/> :
                        <Typography className={classes.cell} variant='subheading'>{Text.register}</Typography>
                    }
                    {register ? <Button color='primary' type='submit' variant='contained' onClick={() => this.regUser(this.state.username, this.state.password)}>Register user</Button> :
                        <Button color='primary' variant='contained' href={'/register'}>Get user now</Button>
                    }
                    {
                        register && this.state.callback !==null ? <Typography variant='display1' align='center' className={classes.welcome}>{this.state.callback.message || this.state.callback.error}</Typography> : null
                    }
                </forum>
                }
            </Paper>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        signIn: (username: string, password: string) => {
            dispatch(signIn(username, password));
        }
    };
};

const mapStateToProps = state => ({
    user: state.user.items,
    loading: state.user.loading,
    error: state.user.error
});

const InjectionStyles = withStyles(styles)(Forum);

export default connect(mapStateToProps, mapDispatchToProps)(InjectionStyles);

