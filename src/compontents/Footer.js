import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import classNames from "classnames";
import {contactInfo} from '../Textfiles/Footer'

import icon from '../Icons/favicon.ico'

const styles = {
    root: {
        width:'auto',
        height:'auto',
        backgroundColor:'#1a1a1a',
        fontFamily:'arial',
        display:'flex',
        flexDirection:'row',
        alignItems:'flex-start',
        justifyContent:'space-between',
        padding:'0px 30px 0px 0px',
    },
    icon:{
        padding:'30px 0 0 30px',
    },
    cell:{
        width: '300px',
        height: '50%',
        margin: '10px 10px 10px 10px'
    },
    unList:{
        listStyleType:'none',
        overflow: 'hidden',
        paddingTop:'50px'
    },

};


const map = contactInfo.map((info, index) =>
    <li key={index} style={{paddingBottom: '5px', color:'whitesmoke'}}> {info} </li>
);


type Props ={
    classes: Object,
}

const Footer = (props: Props) =>{
        const { classes } = props;
        return (
            <div className={classNames(classes.root, props.className)}>
                <img src={icon} className={classes.icon} width='100px'/>
                    <div className={classes.cell}>
                        <ul className={classes.unList}>
                            {map}
                        </ul>
                    </div>
            </div>
        );
};

export default withStyles(styles)(Footer);
