//@flow
import React, {Component, Fragment} from 'react';
import {withStyles} from '@material-ui/core/styles';
import {fetchCategory} from '../service/api/category'
import {createArticle,editArticle} from "../service/api/Article";
import { withRouter } from 'react-router-dom';

//redux
import {connect} from "react-redux";
import {fetchArticles} from "../actions/ArticleAction";

// Material UI Components
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';



const styles = {
    root: {
        height: 'auto',
        width: 'auto',
        padding:'40px'
    },
    grid: {
        display:'flex',
        flexDirection: 'column',
        justifyContent:'space-evenly',
    },
    cell:{
        display:'flex',
        flexDirection: 'row',
        justifyContent:'center',
    },
    list:{
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        position: 'relative',
        overflow: 'auto',
        maxHeight: 100,
    },
    button:{
        width: '100%'
    }
};


type Props = {
    classes: Object,
    edit: boolean,
    match: Object,
    id: string,
    article: Object,
    fetchArticles: Function,
    history: Object
}

type State = {
    title: string,
    text: string,
    selectedFile: any,
    order: boolean,
    cat: string,
    isLoaded: boolean,
    category: Array<Object>,
    submit: boolean,
    subheader: string,
}

class ArticleRefac extends Component<Props, State> {
    state = {
        title: "",
        text: "",
        selectedFile: "",
        subheader: "",
        order: true,
        cat: "",
        isLoaded: false,
        category: [],
        submit: false
    };

    componentDidMount(){
       const article : Object =  this.props.article;
       console.log(article);
        fetchCategory()
            .then(cats =>
                this.setState({
                    category: cats,
                    isLoaded: true
                })
            ).catch(err => console.log(err));

        if(this.props.edit){
            this.setState({
                title: article.title,
                text: article.text,
                subheader: article.subtitle,
                cat: article.category._id
            })

        }
    }


    filehandler = (event : SyntheticInputEvent<HTMLInputElement>) : void =>{
        this.setState({
            selectedFile: event.target.files[0]
        })
    };

    handleChange = (name : string)  =>(event: SyntheticInputEvent<HTMLInputElement>) : void => {
        this.setState({
            [name]: event.target.value,
        })
    };

    orderHandler = () : void=>{
        this.setState({
            order: !this.state.order
        });
    };

    color = () : string => {
        return this.state.order ? 'primary' : 'secondary'
    };

    changeCat = (id : string) : void =>{
        this.setState({
            cat: id
        })
    };

    fileedit = async () : Promise<void> =>{
        const id = this.props.id;
        let update = [];

         update.push({key:"title", value: this.state.title});
         update.push({key:"subtitle", value: this.state.subheader});
         update.push({key:"text", value: this.state.text});
         update.push({key:"category", value: this.state.cat});

        let result = update.reduce((obj, item) => Object.assign(obj, {[item.key]: item.value}) ,{});

        await editArticle(id,result);
        await this.props.fetchArticles();
        await this.props.history.push('/article/'+ this.props.id);
    };

    fileUploader = async ()  : Promise<void> => {
        let article : Object;
        if(this.state.cat !== "") {
          let priority = this.state.order ? "1" : "2";
          let formData = new FormData();
          formData.append("title", this.state.title);
          formData.append("text", this.state.text);
          formData.append("subtitle", this.state.subheader);
          formData.append("order", priority);
          formData.append("category", this.state.cat);
          formData.append("articleImage", this.state.selectedFile);
          await createArticle(formData).then(e => e.json()
          ).then(json =>{
            this.props.fetchArticles();
            article = json.create;
            this.props.history.push('/article/' + article._id)
          });
        }


    };

    render() {
        const {classes, edit} : Object = this.props;

        let buttonClass : string = this.state.order ? 'primary' : 'secondary';

        return (
            <Paper className={classes.root}>
                {
                    this.state.isLoaded ?
                        <form className={classes.grid} onSubmit={this.fileUploader}>
                        <Typography align='center' variant='display2'> {edit? "Edit post" : "Create post"}</Typography>
                        <br/>
                        <Typography variant='display1'>Title:</Typography>
                        <TextField
                            required={!edit}
                            value={this.state.title}
                            label="Title:"
                            style={{ margin: 8 }}
                            placeholder="Please write the title here"
                            fullWidth
                            margin="normal"
                            onChange={this.handleChange("title")}
                        />
                        <Typography variant='display1'>Subheader:</Typography>
                        <TextField
                            required={!edit}
                            value={this.state.subheader}
                            label="subheader:"
                            style={{ margin: 8 }}
                            placeholder="Please write the subheader here"
                            fullWidth
                            margin="normal"
                            onChange={this.handleChange("subheader")}
                        />
                        <Typography variant='display1'>Text:</Typography>
                        <TextField
                            required={!edit}
                            value={this.state.text}
                            label="Text:"
                            style={{ margin: 8 }}
                            placeholder="write text here"
                            multiline
                            rowsMax="4"
                            fullWidth
                            margin="normal"
                            variant="outlined"
                            onChange={this.handleChange("text")}
                        />

                        {edit?<div/>:<div> <Typography variant='display1'>Picture:</Typography>
                            <input required type='file' onChange={this.filehandler}/></div>}

                        {
                            edit ? null : <Fragment>
                                <Typography variant='display1'>Order:</Typography>
                                <Button onClick={this.orderHandler} variant='contained' color={buttonClass}>{this.state.order ? "Post is important!" : "Post is not Important"}</Button>
                            </Fragment>
                        }


                        <Typography variant='display1'>Category:</Typography>

                        <List className={classes.list}>
                            {this.state.category.map(e => {
                                let id = e._id;
                                return <ListItem key={id} button onClick={() => this.changeCat(id)} selected={this.state.cat === id}>
                                    <ListItemText primary={e.name} secondary={e._id} />
                                </ListItem>
                            })}
                        </List>



                        {
                            edit ?<Button variant='outlined' className={classes.button} onClick={() => this.fileedit()}>Edit post</Button>
                                : <Button variant='outlined'  type={"submit"} className={classes.button}> Create post</Button>
                        }
                    </form>: <div/>
                }
            </Paper>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchArticles: () => {
      dispatch(fetchArticles());
    }
  };
};

const history = withRouter(ArticleRefac);
const styling = withStyles(styles)(history);
export default connect(null,mapDispatchToProps)(styling);
