//@flow
import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import className from 'classnames';
import moment from 'moment-timezone';

// Material UI Components
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
// Icons

const styles = {
    root:{
        width: 'auto',
        height: 'auto',
    },
    wrapper: {
        padding: '30px',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
};

type Props = {
    data: Object,
    classes: Object,
    className: Object
}

const Comment = (props: Props) => {
    const {classes, data} : Object = props;
    const i: Object = moment(data.date);

    return (
        <Paper className={className(classes.root, props.className)}>
            <div className={classes.wrapper}>
                <Typography>
                    {data.text}
                </Typography>
                <div>
                <Typography>
                    {data.user}
                </Typography>
                <Typography>
                    {i.tz('Europe/Oslo').format('DD/MM/YYYY, HH:mm ')}
                </Typography>
                </div>
            </div>
        </Paper>
    );
};


export default withStyles(styles)(Comment);



