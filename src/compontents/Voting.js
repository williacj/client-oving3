//@flow
import React from 'react';
import {withStyles} from '@material-ui/core/styles';

// Material UI Components
import Typography from "@material-ui/core/Typography";
import Button from '@material-ui/core/Button'

// Icons
import Vote from '@material-ui/icons/HowToVoteOutlined';
import connect from "react-redux/es/connect/connect";

const styles = {
    root:{
        display: 'flex',
        flexDirection: 'row',
        justifyContent:'center',
        alignItems:'center',
    },
    mH:{
        margin: '20px 20px'
    }
};
type Props={
    classes: Object,
    article: Object,
    number: number,
    voted: boolean,
    onClick: Function
}

const Voting = (props: Props) => {
    const {classes, article, onClick, voted} = props;
    return (
        <div className={classes.root} >
            <Typography variant='subheading' className={classes.mH}> {article.rate.length} votes</Typography>
            <Button variant="contained" color='primary' disabled={voted} className={classes.button} onClick={onClick}>
                Vote
                <Vote/>
            </Button>
        </div>
    );
};


const InjectionStyles = withStyles(styles)(Voting);

export default connect()(InjectionStyles);
