//@flow
import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import className from 'classnames';
import {commentArticle} from "../service/api/Article";
import {updateAnArticle} from "../actions/ArticleAction";
// Material UI Components
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import {connect} from "react-redux";

const styles = {
    root:{
        width: 'auto',
        height: 'auto',
    },
    wrapper: {
        padding: '30px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    }
};

interface Props {
    classes: Object,
    className: Object,
    id: string,
    article: Object,
    dispatch: Function
}

interface State {
    comment: string,

}

class CommentSection extends Component<Props,State>{
        state  = {
            comment: "",
        };

    commentArticle = async (id: string) : Promise<void> =>{
        await this.setState({
            comment: this.state.comment.trim()
        });
        if(!(this.state.comment === "")){
            await commentArticle(id, this.state.comment);
            await this.props.dispatch(updateAnArticle(this.props.article));
            await this.setState({
                comment: ""
            })
        }
    };

    handleChange = (event : SyntheticInputEvent<HTMLInputElement>) =>{
        this.setState({comment: event.target.value});
    };


    render(){
        const {classes, id} : Props = this.props;

        return (
            <Paper className={className(classes.root, this.props.className)}>
                <form className={classes.wrapper}>
                 <TextField
                    id="commentSection"
                    label="Comment"
                    style={{ margin: 8 }}
                    placeholder="Share your thought with the world"
                    fullWidth
                    margin="normal"
                    onChange={this.handleChange}
                    value={this.state.comment}
                />
                    <Button variant='contained' color='primary' onClick={() =>this.commentArticle(id)}>comment article</Button>
                </form>


            </Paper>
        );
    }
}

const InjectionStyles = withStyles(styles)(CommentSection);

export default connect()(InjectionStyles);
