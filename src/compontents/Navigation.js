import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';

// Project Components
import Footer from './Footer';
import Header from "./Header";


const styles = {

};

type Props = {
    footer: boolean,
    children: Object
}


const Navigation = (props: Props) =>  {
    const { footer } = props;
    return (
        <Fragment>
            <Header />
            <main>
                <div>
                    {props.children}
                    </div>
            </main>
            {footer ? <Footer/> :null}
            </Fragment>
        );
};

export default withStyles(styles)(Navigation);
