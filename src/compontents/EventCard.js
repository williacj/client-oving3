import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {withStyles} from '@material-ui/core/styles';
import classNames from "classnames";

// material ui imports
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent"
import PropTypes from "prop-types";
// Icons import
const url = "http://localhost:8000/";

const styles : Object = {
    root:{
        height: 'auto',
        width:'auto',
        '&:hover':{
            cursor: 'pointer',
            backgroundColor:'#E0E0E0',

        }
    },
    media: {
        maxHeight: 400,
        objectFit: 'cover'
    },
    text: {
        textDecoration: 'none',
        textColor: 'black',
        '&:hover':{
            textDecoration: 'underline',

        }
    }
};
type Props = {
    classes: Object,
    data: Object,
    title: string
}

const EventCard = (props : Props) =>  {
    const {classes, data} = props;
    return (
        <Card className={classNames(classes.root, props.className)}>
            <Link to={'/article/' + data._id} style={{ textDecoration: 'none' }}>
            <CardMedia
                className={classes.media}
                component="img"
                image={url+data.articleImage}
                title={data.articleImage}
            />
            <CardContent>
                <Typography gutterBottom variant={props.title} className={classes.text}>
                    {data.title}
                    </Typography>
            </CardContent>
            </Link>
        </Card>
    );
};

export default withStyles(styles)(EventCard);
