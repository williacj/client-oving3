// @flow
const url = 'http://localhost:8000/article/';


export function fetchArticle(id : string) : Promise<Object>{
    return fetch(url + id)
        .then(res => res.json())
        .then(json => json.create)
}

export function deleteArticle(id : string) : Promise<Object>{
    return fetch(url + id, {
        method: 'DELETE',
        headers: {"Authorization": "Bearer " + window.localStorage.getItem('auth')},
    }).then(response =>
        response.json()).then(json => {
            return json;
        })
}

export function commentArticle(id : string, comment: string) : Promise<Response>{
    return fetch(url + id, {
        method: 'PATCH',
        headers: {
            "Authorization": "Bearer " + window.localStorage.getItem('auth'),
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({text : comment})
    });
}


export function voteArticle(id : string): Promise<Object>{
    console.log(window.localStorage.getItem('auth'));
    return fetch(url+ '/vote/' + id, {
        method: 'PATCH',
        headers: {
            "Authorization": "Bearer " + window.localStorage.getItem('auth'),
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ })
    }).then(response =>
        response.json()).then(json => {
        return json;
    });
}

export function createArticle(article : FormData) :  Promise<Response>{
    return fetch((url),{
        method: "POST",
        body: article,
        headers: {"Authorization": "Bearer "+window.localStorage.getItem('auth')},
    })
}

export function editArticle(id: string, result: Object) : Promise<Response>{
      return fetch((url+"updateArticle/"+id),{
        method: "PATCH",
        body: JSON.stringify(result),
        headers: {"Authorization": "Bearer "+window.localStorage.getItem('auth'),
            'Content-Type': 'application/json',},
    })
}
