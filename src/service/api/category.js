// @flow
const url = "http://localhost:8000/category/";

export function fetchCategory () : Promise<Array<Object>> {
       return fetch(url)
            .then(res => res.json())
            .then(json => json.categorys)
}
