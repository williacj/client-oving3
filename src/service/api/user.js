// @flow
const url  : string = "http://localhost:8000/token/";
const url2 : string = "http://localhost:8000/user/";

export function fetchUser() : Promise<Object> {
    return fetch(url,{
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + window.localStorage.getItem('auth')
        },
    })
}

export function registerUser(username: string, password: string): Promise<Object>{
    return fetch((url2 + 'signup'),{
        method: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({name: username, password: password})
    }).then(res => res.json())
}
