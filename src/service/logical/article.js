//@flow

export function filterArticles(loading: boolean, param : string, articles : Array<Object>) : Array<Object> {
    if(!loading && typeof param !== 'undefined'){
        const name = param;
      return articles.filter(e => e.category.name === name);
    }else{
       return articles.filter(e => e.order === 1);
    }
}
