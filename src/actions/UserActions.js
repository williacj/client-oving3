//@flow

/*This class handles action User towards the store */
export const FETCH_USER_BEGIN : string  = 'FETCH_USER_BEGIN';
export const FETCH_USER_SUCCSESS : string = 'FETCH_USER_SUCCSESS';
export const FETCH_USER_FAILURE : string = 'FETCH_USER_FAILURE';

const url : string = "http://localhost:8000/user/";

// Unsafe to store tokens on local user

export function signIn(username: string, password: string) : Function {
    return (dispatch: Function) => {
        dispatch(fetchProductsBegin());
        return fetch((url + 'signin'),{
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({name: username, password: password})
                }).then(handleErrors)
                .then(res => res.json())
                .then(json => {
                  console.log(json);
                    window.localStorage.setItem('auth', json.token);
                    dispatch(fetchuserSuccess(json));
                    return json;
                })
                .catch(error => fetchUserFailure(error));
    };
}

export const fetchProductsBegin = () : Object => ({
    type: FETCH_USER_BEGIN
});

export const fetchuserSuccess = (user : Object): Object => ({
    type: FETCH_USER_SUCCSESS,
    payload: { user }
});

export const fetchUserFailure = (error: Object) : Object=> ({
    type: FETCH_USER_FAILURE,
    payload: { error }
});

// Handle HTTP errors since fetch won't.
function handleErrors(response: Response) : Response {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}
