//@flow
/*This class handles action Articles towards the store */
export const FETCH_ARTICLES_BEGIN : string  = 'FETCH_ARTICLES_BEGIN';
export const FETCH_ARTICLES_SUCCESS : string = 'FETCH_ARTICLES_SUCCESS';
export const FETCH_ARTICLES_FAILURE : string= 'FETCH_ARTICLES_FAILURE';
export const UPDATE_ARTICLE : string= 'UPDATE_ARTICLE';
export const DELETE_ARTICLE : string= 'DELETE_ARTICLE';

const url : string= "http://localhost:8000/article/";

export function fetchArticles() : Function{
    return (dispatch: Function) => {
        dispatch(fetchProductsBegin());
        return fetch(url)
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetchProductsSuccess(json.response.articles));
                return json.response.articles;
            })
            .catch(error => {
                dispatch(fetchProductsFailure(error))});
    };
}

export function updateAnArticle(article: Object): Function {
    return (dispatch: Function) => {
        return fetch(url+ article._id)
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(updateOneproduct(json.create));
                return json.create;
            })
            .catch(error => {
                console.log(error)});
    };
}

export function delArt(article : Object): Function{
    return (dispatch: Function)=>{
         dispatch(deleteAnArtice(article));
    }
}

export const fetchProductsBegin = (): Object => ({
    type: FETCH_ARTICLES_BEGIN
});

export const fetchProductsSuccess = (articles: Array<Object>): Object => ({
    type: FETCH_ARTICLES_SUCCESS,
    payload: { articles }
});

export const deleteAnArtice = (article: Object): Object => ({
    type: DELETE_ARTICLE,
    payload: {article }
});

export const updateOneproduct = (article: Object): Object => ({
    type: UPDATE_ARTICLE,
    payload: { article },
});

export const fetchProductsFailure = (error : Object): Object => ({
    type: FETCH_ARTICLES_FAILURE,
    payload: { error }
});


function handleErrors(response): Object {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}
