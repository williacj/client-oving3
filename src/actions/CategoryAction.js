//@flow

/*This class handles category actions towards the store */
export const FETCH_CATEGORY_BEGIN : string   = 'FETCH_CATEGORY_BEGIN';
export const FETCH_CATEGORY_SUCCESS : string = 'FETCH_CATEGORY_SUCCESS';
export const FETCH_CATEGORY_FAILURE : string = 'FETCH_CATEGORY_FAILURE';

const url : string = "http://localhost:8000/category/";

export function fetchCategory() : Function {
    return (dispatch: Function) => {
        dispatch(fetchCategoryBegin());
        return fetch(url)
            .then(handleErrors)
            .then(res => res.json())
            .then(json => {
                dispatch(fetchCategorySuccess(json.categorys));
                return json.categorys;
            })
            .catch(error => {
                dispatch(fetchCategoryFailure(error))});
    };
}

export const fetchCategoryBegin = () : Object => ({
    type: FETCH_CATEGORY_BEGIN
});

export const fetchCategorySuccess = (category : Array<Object>) : Object => ({
    type: FETCH_CATEGORY_SUCCESS,
    payload: { category }
});

export const fetchCategoryFailure = (error: Object)  : Object => ({
    type: FETCH_CATEGORY_FAILURE,
    payload: { error }
});


// Handle HTTP errors since fetch won't.
function handleErrors(response : Response) : Object {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}
